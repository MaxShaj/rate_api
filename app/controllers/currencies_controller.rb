class CurrenciesController < ApplicationController
  before_action :authorize_request
  before_action :find_currency, only: [:show]

  def index
    @currencies = if params[:page]
                    Currency.paginate(page: params[:page], per_page: 10)
                  else
                    Currency.all
                  end

    render json: @currencies
  end

  def show
    render json: @currency.rate
  end

  private

  def find_currency
    @currency = Currency.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render json: { errors: 'Currency not found' }, status: :not_found
  end
end
