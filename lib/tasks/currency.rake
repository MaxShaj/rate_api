namespace :currency do
  desc "Update rate in currencies"
  task update: :environment do
    xml = open('http://www.cbr.ru/scripts/XML_daily.asp').read

    Hash.from_xml(xml)['ValCurs']['Valute'].each do |valute|
      currency = ::Currency.find_or_create_by(name: valute['Name'])
      currency.update_attributes!(rate: valute['Value'].gsub(',', '.').to_f)
    end
  end
end
