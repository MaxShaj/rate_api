require 'jwt'

module ApiHelper
  include Rack::Test::Methods

  # SECRET_KEY = Rails.application.secrets.secret_key_base.to_s

  def authenticated_header(user_id)
    token = JsonWebToken.encode(user_id: user_id)
    # token = JWT.encode(user_id , SECRET_KEY)
    { 'Authorization': "Bearer #{token}" }
  end

  def app
    Rails.application
  end
end
