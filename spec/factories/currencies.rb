FactoryBot.define do
  factory :currency do
    name { Faker::Lorem.characters(number: 5) }
    rate { Faker::Number.decimal(l_digits: 2) }
  end
end
