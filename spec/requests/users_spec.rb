require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  include ApiHelper

  let!(:users) { create_list(:user, 10) }
  let(:user_id) { users.first.id }
  let(:username) { users.first.username }

  describe 'GET /users when the request with NO authentication header' do
    before { get '/users' }

    it 'returns status code 401' do
      expect(response).to have_http_status(401)
    end
  end

  describe 'GET /users' do
    before { get '/users', params: {}, headers: authenticated_header(user_id) }

    it 'returns users' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /users/:username' do
    before { get "/users/#{username}",
                 params: {},
                 headers: authenticated_header(user_id) }

    context 'when the record exists' do
      it 'returns the user' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(user_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:username) { 'abc' }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/User not found/)
      end
    end
  end

  describe 'POST /users' do
    let (:password) { Faker::Internet.password }
    let (:name) { Faker::Name.first_name }
    let(:valid_attributes) do
      { name: name,
        username: Faker::Internet.user_name,
        email: Faker::Internet.email,
        password: password,
        password_confirmation: password }
    end

    context 'when the request is valid' do
      before { post '/users',
                    params: valid_attributes,
                    headers: authenticated_header(user_id) }

      it 'creates a user' do
        expect(json['name']).to eq(name)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/users',
                    params: { name: name },
                    headers: authenticated_header(user_id) }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/[\"Password can't be blank\",\"Password is too short (minimum is 6 characters)\",\"Email can't be blank\",\"Email is invalid\",\"Username can't be blank\"]/)
      end
    end
  end

  describe 'PUT /users/:username' do
    let(:valid_attributes) { { email: Faker::Internet.email } }

    context 'when the record exists' do
      before { put "/users/#{username}",
                   params: valid_attributes,
                   headers: authenticated_header(user_id) }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /users/:username' do
    before { delete "/users/#{users.last.username}",
                    params: {},
                    headers: authenticated_header(user_id) }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
