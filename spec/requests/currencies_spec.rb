require 'rails_helper'

RSpec.describe 'Currencies API', type: :request do
  include ApiHelper

  let!(:currencies) { create_list(:currency, 15) }
  let (:user_id) { create(:user).id }
  let (:id) { currencies.first.id }

  describe 'GET /currencies when the request with NO authentication header' do
    before { get '/currencies' }

    it 'returns status code 401' do
      expect(response).to have_http_status(401)
    end
  end

  describe 'GET /currencies without page params' do
    before do
      get '/currencies',
          params: {},
          headers: authenticated_header(user_id)
    end

    it 'returns currencies' do
      expect(json).not_to be_empty
      expect(json.size).to eq(15)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /currencies with page params' do
    before do
      get '/currencies',
          params: { page: 1 },
          headers: authenticated_header(user_id)
    end

    it 'returns currencies' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /currency/:id' do
    before do
      get "/currency/#{id}",
          params: {},
          headers: authenticated_header(user_id)
    end

    context 'when the record exists' do
      it 'returns the user' do
        expect(json).to eq(currencies.first.rate)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:id) { Faker::Number.number(digits: 10) }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Currency not found/)
      end
    end
  end
end
