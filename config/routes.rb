Rails.application.routes.draw do
  resources :users, param: :_username

  post '/auth/login', to: 'authentication#login'
  # get '/*a', to: 'application#not_found'

  get '/currency/:id', to: 'currencies#show'
  get '/currencies/:page', to: 'currencies#index'
  get '/currencies', to: 'currencies#index'
end
